import { Component, OnInit } from '@angular/core';

@Component({
  // selector: '[app-servers]', <div app-servers></div> to render it as an attribute
  // selector: '.app-servers', // <div class="app-servers"></div> to render it as an attribute
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = true;
  serverCreationStatus = 'No server created';
  serverName = 'test';
  serverCreated = false;
  servers = ['Test server', 'test server 2'];

  constructor() { }

  ngOnInit(): void {
  }

  // on - triggered from template, some event will call it
  onCreateServer() {
    this.serverCreated = true;
    this.servers.push(this.serverName);
    this.serverCreationStatus = 'Server was created! Name is ' + this.serverName;
  }

  onUpdateServerName(event: Event) {
    // inform ts that the type of this event will be an HTML input element
    this.serverName = (<HTMLInputElement>event.target).value;
  }

}
