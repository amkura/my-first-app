import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ServerComponent } from './server/server.component';
import { ServersComponent } from './servers/servers.component';

@NgModule({
  // Add components
  declarations: [
    AppComponent,
    ServerComponent,
    ServersComponent
  ],
  // Add modules
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent] // lists the components which should be known to Angular the time of index.html initialization
})
export class AppModule { }
